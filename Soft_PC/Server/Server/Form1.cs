﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;
using System.Net.Sockets;
using System.Threading;

namespace Server
{
    public partial class Form1 : Form
    {

        IPEndPoint ipPoint = null;
        Socket listenSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

        Thread t = null;

        static int port = 8080; // порт для приема входящих запросов
        static int countSensors = 4; // число сенсоров для определения координат
        static string maskNameSens = "Base"; //маска для определения своих сенсоров "Base"

        Target curTarget = new Target(); // текущая цель

        WiFiSensor[] allSensors = null;
        WiFiSensor[] sensorsLoc = null;

        WiFiSensor[] baseStation = new WiFiSensor[4];

        Buffer bufX = new Buffer(10, 10);
        Buffer bufY = new Buffer(10, 10);

        Buffer bufBase1 = new Buffer(10, 10);
        Buffer bufBase2 = new Buffer(10, 10);
        Buffer bufBase3 = new Buffer(10, 10);
        Buffer bufBase4 = new Buffer(10, 10);

        int countMean = 0;
        int countXY = 0;

        public Form1()
        {
            InitializeComponent();

            ipPoint = new IPEndPoint(IPAddress.Parse(textBox6.Text), port);

            InitBaseStation();

           /* WiFiSensor[] fr = new WiFiSensor[2];

            fr[0] = new WiFiSensor("Base3", 600, 500);
            fr[1] = new WiFiSensor("Base2", 600, 50);

            calcLocTarget_3(fr, new Target());*/

            //t = new Thread(Priem);
           // t.Start();
        }

        private void InitBaseStation()
        {
            baseStation[0] = new WiFiSensor("Base1", 60, 50);
            baseStation[0].distM = 500;
            baseStation[0].valueBase = (int)numericUpDown3.Value;
            baseStation[0].valueMax = (int)numericUpDown4.Value;
            baseStation[0].calcGain();

            baseStation[1] = new WiFiSensor("Base2", 560, 50);
            baseStation[1].distM = 500;
            baseStation[1].valueBase = (int)numericUpDown5.Value;
            baseStation[1].valueMax = (int)numericUpDown6.Value;
            baseStation[1].calcGain();

            baseStation[2] = new WiFiSensor("Base3", 560, 550);
            baseStation[2].distM = 500;
            baseStation[2].valueBase = (int)numericUpDown7.Value;
            baseStation[2].valueMax = (int)numericUpDown8.Value;
            baseStation[2].calcGain();

            baseStation[3] = new WiFiSensor("Base4", 60, 550);
            baseStation[3].distM = 500;
            baseStation[3].valueBase = (int)numericUpDown9.Value;
            baseStation[3].valueMax = (int)numericUpDown10.Value;
            baseStation[3].calcGain();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (t == null)
            {
                t = new Thread(Priem);
                t.Start();
            }
            else
            {
                if (t.ThreadState == ThreadState.Running) t.Abort();
            }
        }

        private void Priem()
        {
            try
            {
                // связываем сокет с локальной точкой, по которой будем принимать данные
                listenSocket.Bind(ipPoint);

                // начинаем прослушивание
                listenSocket.Listen(10);

                BeginInvoke(new Action(() => textBox1.AppendText("Сервер запущен. Ожидание подключений...\n")));
                BeginInvoke(new Action(() => textBox1.Refresh()));

                while (true)
                {
                    Socket handler = listenSocket.Accept();
                    BeginInvoke(new Action(() => textBox1.AppendText("Подключен...\n")));
                    BeginInvoke(new Action(() => textBox1.Refresh()));
                    // получаем сообщение
                    StringBuilder builder = new StringBuilder();
                    int bytes = 0; // количество полученных байтов
                    byte[] data = new byte[256]; // буфер для получаемых данных

                    do
                    {
                        bytes = handler.Receive(data);
                        for(int v = 0; v < 6; v++)
                        {
                            Thread.Sleep(100);
                            Application.DoEvents();
                        }
                        
                        builder.Append(Encoding.ASCII.GetString(data, 0, bytes));
                        Application.DoEvents();
                    }
                    while (handler.Available > 0);

                    BeginInvoke(new Action(() => textBox1.AppendText(DateTime.Now.ToString("hh:mm:ss") + "\n")));
                    BeginInvoke(new Action(() => textBox1.AppendText(builder.ToString() + "\n")));
                    BeginInvoke(new Action(() => textBox1.AppendText("\n")));
                    BeginInvoke(new Action(() => textBox1.Refresh()));
                    ParseSense(builder.ToString());
                    sensorsLoc = sortSensers(allSensors, countSensors, maskNameSens);
                    refreshCalcSensor(sensorsLoc);

                    int b1 = setBaseCalibr("Base1", sensorsLoc);
                    if (b1 != 999) bufBase1.addVal(b1);
                    int b2 = setBaseCalibr("Base2", sensorsLoc);
                    if (b2 != 999) bufBase2.addVal(b2);
                    int b3 = setBaseCalibr("Base3", sensorsLoc);
                    if (b3 != 999) bufBase3.addVal(b3);
                    int b4 = setBaseCalibr("Base4", sensorsLoc);
                    if (b4 != 999) bufBase4.addVal(b4);

                    calcLocTarget_3(sensorsLoc, curTarget);

                    visualShow(curTarget, radioButton1);

                    // отправляем ответ
                    //string message = "ваше сообщение доставлено";
                    //data = Encoding.Unicode.GetBytes(message);
                    //handler.Send(data);
                    // закрываем сокет
                    handler.Shutdown(SocketShutdown.Both);
                    handler.Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void ParseSense(string str)
        {
            try
            {
                string[] split = str.Split(new char[] { '\n' });

                allSensors = new WiFiSensor[split.Length - 1];

                int i = 0;
                foreach (string element in split)
                {
                    if (!string.IsNullOrWhiteSpace(element))
                    {
                        allSensors[i] = new WiFiSensor("Noname", 0, 0);
                        allSensors[i].name = element.Substring(element.IndexOf(":") + 2, element.LastIndexOf(" ") - 3);
                        int e = element.IndexOf("(") + 1;
                        allSensors[i].value = Convert.ToInt16(element.Substring(e, e - element.IndexOf("(") + 2));
                        i++;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "4");
            }
        }

        private WiFiSensor[] sortSensers(WiFiSensor[] allSenser, int count, string mask)
        {
            WiFiSensor[] retSensors = new WiFiSensor[count];

            try
            {
                for (int i = 0; i < allSenser.Length; i++)
                {
                    WiFiSensor sens = setBase(allSenser[i].name);
                    setParamBase(allSenser[i], sens);
                    allSenser[i].calcGain();
                    allSenser[i].calcRadius();
                }
                WiFiSensor temp;
                for (int i = 0; i < allSenser.Length - 1; i++)
                {
                    for (int j = i + 1; j < allSenser.Length; j++)
                    {
                        if ((allSenser[i].r > -1) && (allSenser[i].r > allSenser[j].r))
                        {
                            temp = allSenser[i];
                            allSenser[i] = allSenser[j];
                            allSenser[j] = temp;
                        }
                    }
                }
                int k = count - 1;
                for (int i = allSenser.Length - 1;  (i >= 0) && (k >= 0); i--)
                {
                    if (!string.IsNullOrWhiteSpace(mask)) { if (allSenser[i].name.IndexOf(mask) != -1) { retSensors[k] = allSenser[i]; k--; } }
                    else { retSensors[k] = allSenser[i]; k--; }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "3");
            }

            return retSensors;
        }

        private void setParamBase(WiFiSensor _sens, WiFiSensor _base)
        {
            if ((_sens != null) && (_base != null))
            {
                _sens.id = _base.id;
                _sens.X = _base.X;
                _sens.Y = _base.Y;
                _sens.valDistdB = _base.valDistdB;
                _sens.valueBase = _base.valueBase;
                _sens.valueMax = _base.valueMax;
                _sens.distM = _base.distM;
                _sens.calcGain();
            }
        }

        private void calcLocTarget_3(WiFiSensor[] _sensorsLoc, Target _target)
        {
            try
            {
              /*  _sensorsLoc[0].name = "Base3";
                _sensorsLoc[0].value = -25;
                _sensorsLoc[1].name = "Base2";
                _sensorsLoc[1].value = -71;
                _sensorsLoc[2].name = "Base4";
                _sensorsLoc[2].value = -71;*/

                int _X = 60;
                int _Y = 50;

                if ((_sensorsLoc[0] != null) && (_sensorsLoc[1] != null) && (_sensorsLoc[2] != null))
                {
                    WiFiSensor sens = _sensorsLoc[0];

                    if (_sensorsLoc[0].value > sens.valueBase)
                    {
                        sens.value = sens.valueBase;
                        int _v1 = sens.valueMax;
                        sens.valueMax = sens.valueMax + (_sensorsLoc[0].valueBase - _sensorsLoc[0].value);
                        sens.calcGain();
                        sens.valueMax = _v1;
                    }
                    else sens.value = _sensorsLoc[0].value;
                    int _X1 = sens.X;
                    int _Y1 = sens.Y;
                    double _R1 = sens.calcRadius();

                    sens = _sensorsLoc[1];
                    if (_sensorsLoc[1].value > sens.valueBase)
                    {
                        sens.value = sens.valueBase;
                        int _v1 = sens.valueMax;
                        sens.valueMax = sens.valueMax + (_sensorsLoc[1].valueBase - _sensorsLoc[1].value);
                        sens.calcGain();
                        sens.valueMax = _v1;
                    }
                    else sens.value = _sensorsLoc[1].value;
                    int _X2 = sens.X;
                    int _Y2 = sens.Y;
                    double _R2 = sens.calcRadius();

                    double deltaR = Math.Abs(_R1) + Math.Abs(_R2);

                    double _D = Math.Sqrt(Math.Pow(_X1 - _X2, 2) + Math.Pow(_Y1 - _Y2, 2));

                    if (_D < deltaR)
                    {
                        double _A = (Math.Pow(_R1, 2) - Math.Pow(_R2, 2) + Math.Pow(_D, 2)) / (2 * _D);
                        double _H = Math.Sqrt(Math.Abs(Math.Pow(_R1, 2) - Math.Pow(_A, 2)));

                        double _XX = _X1 + _A * (_X2 - _X1) / _D;
                        double _YY = _Y1 + _H * (_Y2 - _Y1) / _D;

                        _X = Convert.ToInt16(_XX - _H * Math.Abs(_Y2 - _Y1) / _D);
                        _Y = Convert.ToInt16(_YY + _H * Math.Abs(_X2 - _X1) / _D);
                    }
                    else
                    {
                        double _S = Math.Abs(Math.Sqrt(Math.Pow(_X1 - _X2, 2) + Math.Pow(_Y1 - _Y2, 2)) - _R1 - _R2);
                        double _RS1 = _R1 + _S / 2;
                        double _RS2 = _R2 + _S / 2;

                        double _Z = _RS1 / _RS2;

                        _X = Convert.ToInt16((_X1 + _Z * _X2) / (1 + _Z));
                        _Y = Convert.ToInt16((_Y1 + _Z * _Y2) / (1 + _Z));
                    }
                    sens = _sensorsLoc[2];
                    if (_sensorsLoc[2].value > sens.valueBase)
                    {
                        sens.value = sens.valueBase;
                        int _v1 = sens.valueMax;
                        sens.valueMax = sens.valueMax + (_sensorsLoc[2].valueBase - _sensorsLoc[2].value);
                        sens.calcGain();
                        sens.valueMax = _v1;
                    }
                    else sens.value = _sensorsLoc[2].value;
                    int _X3 = sens.X;
                    int _Y3 = sens.Y;
                    double _R3 = sens.calcRadius();
                    double _LXY = Math.Sqrt(Math.Pow(_X - _X3, 2) + Math.Pow(_Y - _Y3, 2));

                    if (_LXY < _R3)
                    {
                        double _S = _R3 - _LXY;
                        double _Z = (_S / 2) / _LXY;

                        _X = Convert.ToInt16((_X * (1 + _Z) - _Z * _X3));
                        _Y = Convert.ToInt16((_Y * (1 + _Z) - _Z * _Y3));
                    }
                    else
                    {
                        double _S = _LXY - _R3;
                        double _Z = _S / _R3;

                        double _XR = (_X + _Z * _X3) / (1 + _Z);
                        double _YR = (_Y + _Z * _Y3) / (1 + _Z);
                        _X = Convert.ToInt16((_X + _XR) / 2);
                        _Y = Convert.ToInt16((_Y + _YR) / 2);
                    }

                    if (_X < 50) _X = 60;
                    if (_X > 570) _X = 560;
                    if (_Y < 40) _Y = 50;
                    if (_Y > 560) _Y = 550;

                    bufX.addVal(_X);
                    _target.X = bufX.getMeanVal();
                    bufY.addVal(_Y);
                    _target.Y = bufY.getMeanVal();
                    countXY++;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + "\r\n", "calcLocTarget_3");
            }
        }

        private void visualShow(Target _target, RadioButton _control)
        {
            try
            {
                BeginInvoke(new Action(() => _control.Location = new Point(_target.X, _target.Y)));
                BeginInvoke(new Action(() => label3.Text = _target.X.ToString()));
                BeginInvoke(new Action(() => label5.Text = _target.Y.ToString()));
                countMean++;
                BeginInvoke(new Action(() => label9.Text = countMean.ToString()));
                
                BeginInvoke(new Action(() => label12.Text = countXY.ToString()));

                BeginInvoke(new Action(() => textBox3.Text = bufBase1.getMeanVal().ToString()));
                BeginInvoke(new Action(() => textBox4.Text = bufBase2.getMeanVal().ToString()));
                BeginInvoke(new Action(() => textBox5.Text = bufBase3.getMeanVal().ToString()));
                BeginInvoke(new Action(() => textBox7.Text = bufBase4.getMeanVal().ToString()));

                BeginInvoke(new Action(() => Refresh()));
            }
            catch (Exception ex)
            {
                MessageBox.Show("1", ex.Message);
            }
        }

        private void refreshCalcSensor(WiFiSensor[] _senLoc)
        {
            BeginInvoke(new Action(() => textBox2.AppendText("\n")));
            BeginInvoke(new Action(() => textBox2.AppendText(DateTime.Now.ToString("hh:mm:ss") + "\n")));
            string str = "";
            for (int j = 0; j < _senLoc.Length; j++)
            {
                if (_senLoc[j] != null) str += (j + 1).ToString() + ": " + _senLoc[j].name + " : " + _senLoc[j].value + "\r\n";
            }
            BeginInvoke(new Action(() => textBox2.AppendText(str + "\n\n")));
        }

        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {
            bufX.setCountMean((int)numericUpDown1.Value);
            bufY.setCountMean((int)numericUpDown1.Value);
        }

        private void timer1_Tick(object sender, EventArgs e)
        {

        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            Properties.Settings.Default.Save();
        }

        private int setBaseCalibr(string _name, WiFiSensor[] station)
        {
            foreach (WiFiSensor s in station)
            {
                if (s!= null) if ((_name == s.name) || (_name + " " == s.name) ) return s.value;
            }
            return 999;
        }

        private void numericUpDown2_ValueChanged(object sender, EventArgs e)
        {
            bufBase1.setCountMean((int)numericUpDown2.Value);
            bufBase2.setCountMean((int)numericUpDown2.Value);
            bufBase3.setCountMean((int)numericUpDown2.Value);
            bufBase4.setCountMean((int)numericUpDown2.Value);
        }

        private void numericUpDown3_ValueChanged(object sender, EventArgs e)
        {
            baseStation[0].valueBase = (int)numericUpDown3.Value;
            baseStation[0].calcGain();
        }

        private void numericUpDown4_ValueChanged(object sender, EventArgs e)
        {
            baseStation[0].valueMax = (int)numericUpDown4.Value;
            baseStation[0].calcGain();
        }

        private void numericUpDown5_ValueChanged(object sender, EventArgs e)
        {
            baseStation[1].valueBase = (int)numericUpDown5.Value;
            baseStation[1].calcGain();
        }

        private void numericUpDown6_ValueChanged(object sender, EventArgs e)
        {
            baseStation[1].valueMax = (int)numericUpDown6.Value;
            baseStation[1].calcGain();
        }

        private void numericUpDown7_ValueChanged(object sender, EventArgs e)
        {
            baseStation[2].valueBase = (int)numericUpDown7.Value;
            baseStation[2].calcGain();
        }

        private void numericUpDown8_ValueChanged(object sender, EventArgs e)
        {
            baseStation[2].valueMax = (int)numericUpDown8.Value;
            baseStation[2].calcGain();
        }

        private void numericUpDown9_ValueChanged(object sender, EventArgs e)
        {
            baseStation[3].valueBase = (int)numericUpDown9.Value;
            baseStation[3].calcGain();
        }

        private void numericUpDown10_ValueChanged(object sender, EventArgs e)
        {
            baseStation[3].valueMax = (int)numericUpDown10.Value;
            baseStation[3].calcGain();
        }

        private WiFiSensor setBase(string _name)
        {
            WiFiSensor ret = new WiFiSensor("", 0, 0);

            switch (_name)
            {
                case "Base1 ": ret = baseStation[0]; break;
                case "Base1": ret = baseStation[0]; break;
                case "Base2 ": ret = baseStation[1]; break;
                case "Base2": ret = baseStation[1]; break;
                case "Base3 ": ret = baseStation[2]; break;
                case "Base3": ret = baseStation[2]; break;
                case "Base4 ": ret = baseStation[3]; break;
                case "Base4": ret = baseStation[3]; break;
            }
            return ret;
        }


    }
}

