﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Server
{
    class Buffer
    {
        public List<int> value;
        public int length = 10;
        public int countMean = 3;
        public int curMean = 0;

        public Buffer(int _len = 10, int _count = 3)
        {
            countMean = _count;
            value = new List<int>(_len);

            for (int i = 0; i < length; i++)
            {
                value.Add(0);
            }
        }

        public void addVal(int _x)
        {
            if (value != null)
            {
                value.RemoveRange(0, 1);
                value.Add(_x);
            }
        }

        void mean()
        {
            int cur = 0;
            for (int i = 0; i < countMean; i++)
            {
                cur += value[length - 1 - i];
            }
            cur /= countMean;

            curMean = cur;
        }

        public int getMeanVal()
        {
            if (value.Count > 0) mean();
            return curMean;
        }

        public int getLastVal(int _x)
        {
            return (value != null) ? value[length - 1] : (-1);
        }

        public void setCountMean(int _coun)
        {
            countMean = _coun;
        }
    }
}
