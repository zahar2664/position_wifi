﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Server
{
    class WiFiSensor
    {
        public int id = 0;
        public string name = "Sensor";
        public int X = 0;
        public int Y = 0;
        public int value = 0;
        public int valueBase = 0;
        public int valueMax = 1;
        public double distM = 0;
        public double valDistdB = 0;
        public double r = -1;
        public double gain = 0;

        public WiFiSensor(string _name, int _X, int _Y, double _distM = -1, double _valDistdB = 15)
        {
            name = _name;
            X = _X;
            Y = _Y;
            distM = _distM;
            valDistdB = valueMax - valueBase;
            calcGain();
        }

        public double calcGain()
        {
            valDistdB = Math.Abs(valueMax - valueBase);

            return (gain = valDistdB / distM);
        }
        public double calcRadius()
        {
            r = Convert.ToInt16((Math.Abs(Math.Abs(value) - Math.Abs(valueBase))) / gain);

            return r;
        }
    }
}
