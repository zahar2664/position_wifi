/*
    This sketch sends a message to a TCP server

*/

#include <ESP8266WiFi.h>
#include <ESP8266WiFiMulti.h>

ESP8266WiFiMulti WiFiMulti;
IPAddress ip(192,168,31,7);  //статический IP
IPAddress gateway(192,168,31,1);
IPAddress subnet(255,255,0,0);

void setup() {
  Serial.begin(115200);
  delay(10);

  // We start by connecting to a WiFi network
  WiFi.mode(WIFI_STA);
  WiFiMulti.addAP("Efir_64", "12121987");
  WiFi.config(ip, gateway, subnet);

  Serial.println();
  Serial.println();
  Serial.print("Wait for WiFi... ");

  while (WiFiMulti.run() != WL_CONNECTED) {
    Serial.print(".");
    delay(500);
  }

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());

  delay(500);
}

  // Use WiFiClient class to create TCP connections
  WiFiClient client;
  const uint16_t port = 8080;
  const char * host = "192.168.31.254"; // ip AP
  
void scan(){
    Serial.println("scan start");

  // WiFi.scanNetworks will return the number of networks found
  int n = WiFi.scanNetworks();
  Serial.println("scan done");
  if (n == 0) {
    Serial.println("no networks found");
  } else {
    Serial.print("connecting to ");
    Serial.println(host);

    if (!client.connect(host, port)) {
      Serial.println("connection failed");
      Serial.println("wait 5 sec...");
      delay(5000);
      return;
    }
    Serial.print(n);
    Serial.println(" networks found");
    for (int i = 0; i < n; ++i) {
      // Print SSID and RSSI for each network found
      Serial.print(i + 1);
      Serial.print(": ");
      Serial.print(WiFi.SSID(i));
      Serial.print(" (");
      Serial.print(WiFi.RSSI(i));
      Serial.print(")");
      Serial.println((WiFi.encryptionType(i) == ENC_TYPE_NONE) ? " " : "*");
      delay(10);
      client.print(i + 1);
      client.print(": ");
      client.print(WiFi.SSID(i));
      client.print(" (");
      client.print(WiFi.RSSI(i));
      client.print(")");
      client.println((WiFi.encryptionType(i) == ENC_TYPE_NONE) ? " " : "*");
    }
  }
  Serial.println("");

  // Wait a bit before scanning again
  delay(5000);
}
void loop() {

  scan();

  // This will send the request to the server
  //client.println("Send this data to server");

  //read back one line from server
  //String line = client.readStringUntil('\r');
  //Serial.println(line);

  Serial.println("closing connection");
  client.stop();

  Serial.println("wait 5 sec...");
  delay(5000);
}
